# Package

version       = "0.1.0"
author        = "Jay"
description   = "A new awesome nimble package"
license       = "MIT"
srcDir        = "src"
bin           = @["capsule"]


# Dependencies

requires "nim >= 1.4.2"
