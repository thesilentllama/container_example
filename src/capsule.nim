import os
import posix

const CLONE_NEWNS = 0x00020000      # New mount namespace group
const CLONE_NEWCGROUP = 0x02000000  # New cgroup namespace (4.6+ kernels only)
const CLONE_NEWUTS = 0x04000000     # New utsname namespace
const CLONE_NEWIPC = 0x08000000     # New ipc namespace
const CLONE_NEWUSER = 0x10000000    # New user namespace
const CLONE_NEWPID = 0x20000000     # New pid namespace
const CLONE_NEWNET = 0x40000000     # New network namespace

# Magic numbers
const SYS_PIVOT_ROOT = 155
const MNT_DETACH = 2
# from sys/mount.h
const MS_BIND = 4096
const MS_REC = 16384

proc c_unshare(flag: int): int {.header: "<sched.h>", importc: "unshare"}
proc c_mount(src: cstring,
             target: cstring,
             fs_type: cstring,
             flags: clong,
             data: cstring): int {.header: "<sys/mount.h>", importc: "mount"}
proc c_umount2(target: cstring,
               flags: clong): int {.header: "<sys/mount.h>", importc: "umount2"}
proc c_syscall(number: cint): int {.header: "<syscall.h>", importc: "syscall", varargs.}

template fail_if(cond: typed, reason: string) =
  if cond:
    echo reason
    if errno != 0:
        echo posix.strerror(errno)
    system.quit(1)

proc bind_mount(src: string, dst: string): int =
  return c_mount(src, dst, nil, MS_BIND or MS_REC, nil)

proc unshare(flag: int) =
    fail_if(c_unshare(flag) < 0): "Unable to unshare namespace"

proc chroot(new_root: string) =
  fail_if(bind_mount(new_root, new_root) < 0): "Failed to bind to new filesystem"
  fail_if(posix.chdir(new_root) < 0): "Failed to change to new folder"

  if not os.dirExists("oldroot"):
    os.createDir("oldroot")

  # We're ready to pivot:
  fail_if(c_syscall(SYS_PIVOT_ROOT,
                    new_root,
                    "oldroot") < 0):
    "Unable to pivot_root"

  fail_if(c_mount("proc", "proc", "proc", 0, nil) < 0): "Unable to mount /proc"
  fail_if(c_umount2("oldroot", MNT_DETACH) < 0): "Unable to detach old root"
    
  os.removeDir("oldroot")
  fail_if(posix.chdir("/") < 0): "Failed to switch to new root filesystem"

when isMainModule:
  let parent_pid = posix.getpid()
  let uid = posix.getuid()
  let gid = posix.getgid()
  unshare(CLONE_NEWUSER or CLONE_NEWUTS or CLONE_NEWNS or CLONE_NEWIPC or CLONE_NEWPID or CLONE_NEWNET)

  writeFile("/proc/" & $(parent_pid) & "/setgroups", "deny")
  writeFile("/proc/" & $(parent_pid) & "/gid_map", "0 " & $(gid) & " 1\n")
  writeFile("/proc/" & $(parent_pid) & "/uid_map", "0 " & $(uid) & " 1\n")

  let pid = posix.fork()

  if pid == 0:
    let mypid = posix.getpid()

    fail_if(posix.setgid(0) < 0): "failed to become root"
    fail_if(posix.setuid(0) < 0): "failed to become root"

    chroot(absolutePath("rootfs"))

    var args_array = allocCStringArray(["/bin/sh"])
    var env_array = allocCStringArray(["PATH=/bin:/sbin:/usr/bin"])
    fail_if(posix.execve("/bin/sh", args_array, env_array) < 0): "Unable to exec in container"
  else:
    var status: cint = 0
    fail_if(posix.waitpid(pid, status, 0) < 0): "Failed to wait for child"
