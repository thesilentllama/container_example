use libc::{
    execve, fork, getgid, getpid, getuid, mount, setgid, setuid, syscall, umount2, unshare, waitpid,
};
use std::env::set_current_dir;
use std::ffi::CString;
use std::fs::{canonicalize, create_dir, remove_dir, write};
use std::path::PathBuf;
use std::ptr::null;

const CLONE_NEWNS: i32 = 0x00020000; //New mount namespace group
const CLONE_NEWUTS: i32 = 0x04000000; // New utsname namespace
const CLONE_NEWIPC: i32 = 0x08000000; // New ipc namespace
const CLONE_NEWUSER: i32 = 0x10000000; // New user namespace
const CLONE_NEWPID: i32 = 0x20000000; // New pid namespace
const CLONE_NEWNET: i32 = 0x40000000; // New network namespace

// Magic numbers
const SYS_PIVOT_ROOT: i64 = 155;
const MNT_DETACH: i32 = 2;
// from sys/mount.h
const MS_BIND: u64 = 4096;
const MS_REC: u64 = 16384;

fn chroot(new_root: PathBuf) -> std::io::Result<()> {
    unsafe {
        let src = CString::new(new_root.to_str().unwrap()).expect("Unable to convert to c string");
        let dst = CString::new(new_root.to_str().unwrap()).expect("Unable to convert to c string");
        let oldroot = CString::new("oldroot").expect("Unable to convert to c string");
        mount(src.as_ptr(), dst.as_ptr(), null(), MS_BIND | MS_REC, null());

        set_current_dir(new_root)?;
        create_dir("oldroot")?;

        if syscall(SYS_PIVOT_ROOT, src.as_ptr(), oldroot.as_ptr()) < 0 {
            return Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                "Unable to pivot",
            ));
        }        

        let proc = CString::new("proc").unwrap();
        mount(proc.as_ptr(), proc.as_ptr(), proc.as_ptr(), 0, null());
        
        if umount2(oldroot.as_ptr(), MNT_DETACH) < 0 {
            return Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                "Unable to unmount old root",
            ));
        }

        remove_dir("oldroot")?;
        set_current_dir("/")?;
    }

    Ok(())
}

fn main() -> std::io::Result<()> {
    let parent_pid = unsafe { getpid() };
    let uid = unsafe { getuid() };
    let gid = unsafe { getgid() };

    unsafe {
        unshare(
            CLONE_NEWUSER | CLONE_NEWUTS | CLONE_NEWNS | CLONE_NEWIPC | CLONE_NEWPID | CLONE_NEWNET,
        );
    }

    write(format!("/proc/{}/setgroups", parent_pid), b"deny\n")?;
    write(
        format!("/proc/{}/gid_map", parent_pid),
        format!("0 {} 1\n", gid),
    )?;
    write(
        format!("/proc/{}/uid_map", parent_pid),
        format!("0 {} 1\n", uid),
    )?;

    let pid = unsafe { fork() };

    if pid == 0 {
        unsafe {
            setgid(0);
            setuid(0);
        }

        chroot(canonicalize("rootfs").expect("Unable to find absolute path for target folder"))?;

        let sh = CString::new("/bin/sh").unwrap();
        let args = vec![sh.as_ptr()];

        let path = CString::new("PATH=/bin:/sbin:/usr/bin").unwrap();
        let env = vec![path.as_ptr()];

        unsafe {
            execve(args[0], args.as_ptr(), env.as_ptr());
        }
    } else {
        unsafe {
            let mut status: i32 = 0;
            waitpid(pid, &mut status, 0);
        }
    }

    Ok(())
}
