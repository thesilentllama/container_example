#!/usr/bin/env python3
import ctypes
import ctypes.util
import errno
import os
import os.path
import sys


# From sched.h
CLONE_NEWNS = 0x00020000      # New mount namespace group
CLONE_NEWCGROUP = 0x02000000  # New cgroup namespace (4.6+ kernels only)
CLONE_NEWUTS = 0x04000000     # New utsname namespace
CLONE_NEWIPC = 0x08000000     # New ipc namespace
CLONE_NEWUSER = 0x10000000    # New user namespace
CLONE_NEWPID = 0x20000000     # New pid namespace
CLONE_NEWNET = 0x40000000     # New network namespace

# Magic numbers
SYS_PIVOT_ROOT = 155
MNT_DETACH = 2
PR_SET_DUMPABLE = 4
# from sys/mount.h
MS_BIND = 4096
MS_REC = 16384

# We'll need system calls from libc:
cdll = ctypes.CDLL(ctypes.util.find_library("c"), use_errno=True)


def bind_mount(src, dst):
    return cdll.mount(ctypes.c_char_p(src.encode("utf-8")),
                      ctypes.c_char_p(dst.encode("utf-8")),
                      None,
                      MS_BIND | MS_REC,
                      None)


def mount(src, dst, mnt_type, flags, data):
    return cdll.mount(ctypes.c_char_p(src.encode("utf-8")),
                      ctypes.c_char_p(dst.encode("utf-8")),
                      ctypes.c_char_p(mnt_type.encode("utf-8")) if mnt_type else None,
                      flags,
                      ctypes.c_char_p(data.encode("utf-8")) if data else None)

def fail_if(cond):
    if cond:
        print(errno.errorcode[ctypes.get_errno()])
        sys.exit(1)        

        
def chroot(new_root):
    fail_if(bind_mount(new_root, new_root) < 0)

    os.chdir(new_root)
    
    if not os.path.exists("oldroot"):
        os.mkdir("oldroot", 0o755)

    fail_if(mount("proc", "proc", "proc", 0, None) < 0)
        
    # Glibc doesn't have a binding for pivot_root for some reason.
    # Need to use a direct syscall:
    fail_if(cdll.syscall(SYS_PIVOT_ROOT,
                         ctypes.c_char_p(new_root.encode("utf-8")),
                         ctypes.c_char_p(b"oldroot")) < 0)

    fail_if(cdll.umount2(ctypes.c_char_p(b"oldroot"), MNT_DETACH) < 0)
    os.rmdir(b"oldroot")
    os.chdir("/")


def unshare(flag):
    fail_if(cdll.unshare(flag) < 0)


if __name__ == "__main__":
    parent_pid = os.getpid()
    uid = os.getuid()
    gid = os.getgid()

    unshare(CLONE_NEWUSER | CLONE_NEWUTS | CLONE_NEWNS | CLONE_NEWIPC | CLONE_NEWPID)

    with open("/proc/%i/setgroups" % parent_pid, "wb") as setgroups:
        setgroups.write(b"deny")

    with open("/proc/%i/gid_map" % parent_pid, "wb") as gid_map:
        gid_map.write(b"0 %i 1\n" % gid)

    with open("/proc/%i/uid_map" % parent_pid, "wb") as uid_map:
        uid_map.write(b"0 %i 1\n" % uid)

    pid = os.fork()

    if pid == 0:
        os.setgid(0)
        os.setuid(0)

        chroot(os.path.abspath("rootfs"))
        
        command = "/bin/sh"
        env = {"PATH": "/bin:/sbin:/usr/bin"}
        os.execve(command.split()[0],
                  command.split(),
                  env)
    else:
        os.waitpid(pid, 0)
